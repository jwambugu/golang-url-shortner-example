# Golang URL Shortener

This project shows how to use the hexagonal architecture in golang. It us part of the tutorial series
by [Tensor Programming](https://youtu.be/rQnTtQZGpg8). The project allows a user to shorten a URL and the retrieve and
navigate to the shortened URL website.

## Requirements

1. [Golang](https://golang.org/)
2. [Redis](https://redis.io/)
3. [MongoDB](https://www.mongodb.com)

Alternatively, you can set up your docker environment and run the project.

## How it works

The project currently supports two databases, Redis and MongoDB. For now, you can use only one database. Navigate to
`main.go`. On the `chooseRedirectRepository` function, update the values of
`mongodb.NewRedirectRepository("mongoDBURIGoesHere", "databaseNameGoesHere", 30)` using your MongoDB credentials. Also,
update `redis.NewRedirectRepository("redisURLGoesHere")`.

If you are going to use Redis, update `chooseRedirectRepository(true)` passing `true` on the main function, if you're
going to use MongoDB update `chooseRedirectRepository(false)` passing `false`

## Creating URL

To add a new URL, make a POST request to `http://localhost:8000/`. Here is a sample request body
`{ url: "https://gitlab.com/jwambugu" }`.

To retrieve a URL, make a GET request to `http://localhost:8000/{code}`




