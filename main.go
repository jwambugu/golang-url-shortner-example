package main

import (
	"fmt"
	"gitlab.com/jwambugu/golang-url-shortner-example/repository/mongodb"
	"gitlab.com/jwambugu/golang-url-shortner-example/repository/redis"
	"gitlab.com/jwambugu/golang-url-shortner-example/shortener"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

type application struct {
	redirectRepository shortener.RedirectRepository
}

func httpPort() string {
	return fmt.Sprintf(":%d", 8000)
}

func chooseRedirectRepository(useRedis bool) (shortener.RedirectRepository, error) {
	if !useRedis {
		repository, err := mongodb.NewRedirectRepository("", "", 30)

		if err != nil {
			return nil, err
		}

		return repository, nil
	}

	repository, err := redis.NewRedirectRepository("redis://localhost:6379")

	if err != nil {
		return nil, err
	}

	return repository, nil
}

func main() {
	redirectRepository, err := chooseRedirectRepository(true)

	if err != nil {
		log.Fatal(err)
	}

	app := &application{
		redirectRepository: redirectRepository,
	}

	errorsChan := make(chan error, 2)

	go func() {
		port := httpPort()

		fmt.Println(fmt.Sprintf("Listening on port %s", port))

		errorsChan <- http.ListenAndServe(port, app.router())
	}()

	go func() {
		c := make(chan os.Signal, 1)

		signal.Notify(c, syscall.SIGINT)

		errorsChan <- fmt.Errorf("%s", <-c)
	}()

	fmt.Printf("Terminated: %s", <-errorsChan)
}
