package redis

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	errs "github.com/pkg/errors"
	"gitlab.com/jwambugu/golang-url-shortner-example/shortener"
	"strconv"
)

type redisRepository struct {
	client *redis.Client
}

var ctx = context.Background()

func newRedisClient(url string) (*redis.Client, error) {
	opts, err := redis.ParseURL(url)

	if err != nil {
		return nil, err
	}

	client := redis.NewClient(opts)

	_, err = client.Ping(ctx).Result()

	if err != nil {
		return nil, err
	}

	return client, nil
}

func (r *redisRepository) generateRedirectKey(code string) string {
	return fmt.Sprintf("redirect:%s", code)
}

func (r *redisRepository) Find(code string) (*shortener.Redirect, error) {
	redirect := &shortener.Redirect{}

	key := r.generateRedirectKey(code)

	data, err := r.client.HGetAll(ctx, key).Result()

	if err != nil {
		return nil, errs.Wrap(err, "repository.Redirect.HGetAll")
	}

	if len(data) == 0 {
		return nil, errs.Wrap(shortener.ErrRedirectNotFound, "repository.Redirect.HGetAll")
	}

	createdAt, err := strconv.ParseInt(data["created_at"], 10, 64)

	if err != nil {
		return nil, errs.Wrap(err, "repository.Redirect.ParseInt")
	}

	redirect.Code = data["code"]
	redirect.URL = data["url"]
	redirect.CreatedAt = createdAt

	return redirect, nil
}

func (r *redisRepository) Store(redirect *shortener.Redirect) error {
	key := r.generateRedirectKey(redirect.Code)

	data := map[string]interface{}{
		"code":       redirect.Code,
		"url":        redirect.URL,
		"created_at": redirect.CreatedAt,
	}

	_, err := r.client.HMSet(ctx, key, data).Result()

	if err != nil {
		return errs.Wrap(err, "repository.Redirect.Store")
	}

	return nil
}

func NewRedirectRepository(url string) (shortener.RedirectRepository, error) {
	repo := &redisRepository{}

	client, err := newRedisClient(url)

	if err != nil {
		return nil, errs.Wrap(err, "repository.NewRedirectRepository")
	}

	repo.client = client
	return repo, nil
}
