package mongodb

import (
	"context"
	errs "github.com/pkg/errors"
	"gitlab.com/jwambugu/golang-url-shortner-example/shortener"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"time"
)

type mongoRepository struct {
	client   *mongo.Client
	database string
	timeout  time.Duration
}

const RedirectsCollection = "redirects"

func newMongoClient(uri string, timeout int) (*mongo.Client, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(timeout)*time.Second)

	defer cancel()

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(uri))

	if err != nil {
		return nil, err
	}

	if err := client.Ping(ctx, readpref.Primary()); err != nil {
		return nil, err
	}

	return client, nil
}

func (r *mongoRepository) Find(code string) (*shortener.Redirect, error) {
	ctx, cancel := context.WithTimeout(context.Background(), r.timeout)

	defer cancel()

	redirect := &shortener.Redirect{}

	collection := r.client.Database(r.database).Collection(RedirectsCollection)

	filter := bson.M{"code": code}

	if err := collection.FindOne(ctx, filter).Decode(&redirect); err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, errs.Wrap(shortener.ErrRedirectNotFound, "repository.Redirect.FindOne")
		}

		return nil, errs.Wrap(err, "repository.Redirect.FindOne")
	}

	return redirect, nil
}

func (r *mongoRepository) Store(redirect *shortener.Redirect) error {
	ctx, cancel := context.WithTimeout(context.Background(), r.timeout)

	defer cancel()

	collection := r.client.Database(r.database).Collection(RedirectsCollection)

	_, err := collection.InsertOne(ctx, bson.M{
		"code":       redirect.Code,
		"url":        redirect.URL,
		"created_at": redirect.CreatedAt,
	})

	if err != nil {
		return errs.Wrap(err, "repository.Redirect.InsertOne")
	}

	return nil
}

func NewRedirectRepository(uri, db string, timeout int) (shortener.RedirectRepository, error) {
	repo := &mongoRepository{
		database: db,
		timeout:  time.Duration(timeout) * time.Second,
	}

	client, err := newMongoClient(uri, timeout)

	if err != nil {
		return nil, errs.Wrap(err, "repository.NewRedirectRepository")
	}

	repo.client = client
	return repo, nil
}
