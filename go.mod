module gitlab.com/jwambugu/golang-url-shortner-example

go 1.15

require (
	github.com/go-chi/chi v1.5.1
	github.com/go-redis/redis/v8 v8.4.4
	github.com/pkg/errors v0.9.1
	github.com/teris-io/shortid v0.0.0-20201117134242-e59966efd125
	github.com/vmihailenco/msgpack/v5 v5.1.0
	go.mongodb.org/mongo-driver v1.4.4
	gopkg.in/dealancer/validate.v2 v2.1.0
)
