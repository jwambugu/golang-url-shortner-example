package main

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"gitlab.com/jwambugu/golang-url-shortner-example/api"
	"gitlab.com/jwambugu/golang-url-shortner-example/shortener"
)

func (app *application) router() *chi.Mux {
	redirectService := shortener.NewRedirectService(app.redirectRepository)
	httpHandler := api.NewHandler(redirectService)

	router := chi.NewRouter()
	router.Use(middleware.RequestID, middleware.RealIP, middleware.Logger, middleware.Recoverer)

	router.Get("/{code}", httpHandler.GetRedirectUrl)
	router.Post("/", httpHandler.CreateRedirectUrl)

	return router
}
