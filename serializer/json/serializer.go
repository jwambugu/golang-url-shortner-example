package json

import (
	"encoding/json"
	errs "github.com/pkg/errors"
	"gitlab.com/jwambugu/golang-url-shortner-example/shortener"
)

type Redirect struct{}

func (r *Redirect) Encode(redirect *shortener.Redirect) ([]byte, error) {
	rawMsg, err := json.Marshal(redirect)

	if err != nil {
		return nil, errs.Wrap(err, "serializer.Redirect.Encode")
	}

	return rawMsg, nil
}

func (r *Redirect) Decode(data []byte) (*shortener.Redirect, error) {
	redirect := &shortener.Redirect{}

	if err := json.Unmarshal(data, redirect); err != nil {
		return nil, errs.Wrap(err, "serializer.Redirect.Decode")
	}

	return redirect, nil
}
