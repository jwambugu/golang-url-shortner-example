package msgpack

import (
	errs "github.com/pkg/errors"
	"github.com/vmihailenco/msgpack/v5"
	"gitlab.com/jwambugu/golang-url-shortner-example/shortener"
)

type Redirect struct{}

func (r *Redirect) Encode(redirect *shortener.Redirect) ([]byte, error) {
	rawMsg, err := msgpack.Marshal(redirect)

	if err != nil {
		return nil, errs.Wrap(err, "serializer.Redirect.Encode")
	}

	return rawMsg, nil
}

func (r *Redirect) Decode(data []byte) (*shortener.Redirect, error) {
	redirect := &shortener.Redirect{}

	if err := msgpack.Unmarshal(data, redirect); err != nil {
		return nil, errs.Wrap(err, "serializer.Redirect.Decode")
	}

	return redirect, nil
}
