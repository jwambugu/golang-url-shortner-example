package shortener

type RedirectSerializer interface {
	Encode(redirect *Redirect) ([]byte, error)
	Decode(data []byte) (*Redirect, error)
}
