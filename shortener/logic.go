package shortener

import (
	"errors"
	errs "github.com/pkg/errors"
	"github.com/teris-io/shortid"
	"gopkg.in/dealancer/validate.v2"
	"time"
)

var (
	ErrRedirectNotFound = errors.New("redirect not found")
	ErrRedirectInvalid  = errors.New("redirect invalid")
)

type redirectService struct {
	redirectRepository RedirectRepository
}

func (r redirectService) Find(code string) (*Redirect, error) {
	return r.redirectRepository.Find(code)
}

func (r redirectService) Store(redirect *Redirect) error {
	if err := validate.Validate(r); err != nil {
		return errs.Wrap(ErrRedirectInvalid, "service.Redirect.store")
	}

	redirect.Code = shortid.MustGenerate()
	redirect.CreatedAt = time.Now().Local().Unix()

	return r.redirectRepository.Store(redirect)
}

func NewRedirectService(repo RedirectRepository) RedirectService {
	return &redirectService{
		redirectRepository: repo,
	}
}
